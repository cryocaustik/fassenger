<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1'], function(){
    Route::post('/response', 'ApiController@response');
});

Route::group(['prefix' => 'd'], function(){
    Route::get('/all', 'ApiController@all');
    Route::get('/dump', 'ApiController@dump');
    Route::get('/dump_j', 'ApiController@dump_json');
});