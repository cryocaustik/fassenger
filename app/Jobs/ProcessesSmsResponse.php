<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Messenger;

class ProcessesSmsResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sms)
    {
        // store sms eloquent record
        $this->sms = $sms;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $category = $this->determine_category();

        if(!$category)
        {
            $this->unknown_command();
        } else
        {
            $this->$category();
        }
    }

    public function determine_category()
    {
        $commands = (object)[
            '!deposited' => 'deposit_complete',
            '!status' => 'status_check',
            '!help' => 'help_needed',
            '!stop' => 'stop_service',
            '!start' => 'resume_service',
        ];
        $identified_category = '';
        foreach($commands as $cmd => $cat)
        {
            if(strpos(strtolower($this->sms->body), $cmd) === 0)
            {
                $identified_category = $cat;
                break;
            }
        }

        return $identified_category;
    }

    public function unknown_command()
    {
        $sms = $this->sms;
        $response = Messenger::send($sms->from, 'unknown command :(');
        Log::debug($response);
    }

    public function deposit_complete()
    {
        // TODO: handle command: deposit_complete
        return false;
    }
    public function status_check()
    {
        // TODO: handle command: status_check
        return false;
    }
    public function help_needed()
    {
        // TODO: handle command: help_needed
        return false;
    }
    public function stop_service()
    {
        // TODO: handle command: stop_service
        return false;
    }
    public function resume_service()
    {
        // TODO: handle command: resume_service
        return false;
    }
}
