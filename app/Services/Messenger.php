<?php
/**
 * Created by PhpStorm.
 * User: cryocaustik
 * Date: 7/4/2018
 * Time: 11:12 AM
 */

namespace App\Services;


use Twilio;

class Messenger
{
    public function send($to, $msg)
    {
        $response = Twilio::message($to, $msg);
        return $response;
    }
}