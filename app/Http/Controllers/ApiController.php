<?php

namespace App\Http\Controllers;

use App\ApiToken;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Twilio;
use App\TextResponse;
use App\HttpDump;


class ApiController extends BaseController
{
    public function all()
    {
        return TextResponse::all();
    }

    public function dump()
    {
        return HttpDump::all();
    }

    public function dump_json()
    {
        $d = HttpDump::latest('created_at')->first();
        return response()->json($d);
    }

    public function response()
    {

        if(!request()->exists('token'))
        {
            return response()->json('missing token', 400);
        }

        $token = request()->input('token');
        if($token != ApiToken::pluck('token')->first())
        {
            return response()->json('invalid token', 401);
        }

        try {
            $r = new TextResponse();
            $r->from = request()->input('From');
            $r->body = request()->input('Body');
            $r->from_country = request()->input('FromCountry');
            $r->from_state = request()->input('FromState');
            $r->sms_status = request()->input('SmsStatus');
            $r->sms_message_sid = request()->input('SmsMessageSid');
            $r->message_sid = request()->input('MessageSid');
            $r->save();
            return response()->json('success', 202);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json('error', 500);
        }
    }
}