<?php

use Illuminate\Database\Seeder;
use App\ApiToken;

class ApiTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t = new ApiToken();
        $t->token = str_random(50);
        $t->save();
    }
}
