<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('from');
            $table->string('body');
            $table->string('from_country');
            $table->string('from_state');
            $table->string('sms_status');
            $table->text('sms_message_sid');
            $table->text('message_sid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_responses');
    }

}
