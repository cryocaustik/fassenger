<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHttpDumpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('http_dumps', function (Blueprint $table) {
            $table->increments('id');
            $table->text('header_dump')->nullable();
            $table->text('body_dump')->nullable();
            $table->text('input_dump')->nullable();
            $table->text('exception_dump')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('http_dumps');
    }
}
